const {mongodb} = require('./config.js')
const mongoose = require('mongoose')

const conexion = mongoose.connect(`mongodb://${mongodb.host}:${mongodb.port}/${mongodb.database}`).then((db)=>{
    console.log('conexion exitosa a la bd')
}).catch((error)=>{
    console.log(error)
})

module.exports = conexion