/*
    * Controllers
*/
//importando require
//importando fetch
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
//importando bd eschema 
const Repositories = require('../models/repositories.js')
const obtener = require('./api/github')

// Obtiene repositorios desde Github y guardarlos en BD
// * localhost:3001/repositorios/
const getRepositorios = async (req, res) => {
    obtener('noermorales98').then(data => {
        res.send(data)
    })
}



//Obtiene repositorios desde la BD
// * localhost:3001/repositorios/obtener
const getRepositoriosBD = (req, res) => {
    Repositories.find({}, (err, result) => {
        if (err) {
            console.log('ocurrio un error al obtener datos de la BD')
        }
        else {
            console.log('Enviando datos obtenidos de la BD')
            res.send(result)
        }
    })
}

//Crea y almacena los repositorios en la BD
// * localhost:3001/repositorios/guardar
const createRepositorios = async (req, res) => {
    let username = req.params.username;
    obtener(username).then(data => {
        let i = 0
        while (data[i] != null) {
            let name = data[i].name
            let html_url = data[i].html_url
            let description = data[i].description
            let lenguages = data[i].lenguages
            let topics = data[i].topics
            if (data[i].name === username) {
                console.log('Omitiendo repositorio')
            }
            else {
                Repositories.findOne({ name: data[i].name }, (err, result) => {
                    if (result === null) {
                        const repositorio = new Repositories({
                            name: name,
                            url: html_url,
                            description: description,
                            lenguages: lenguages,
                            topics: topics
                        })
                        repositorio.save().then(result => {
                            console.log('guardado en la BD')
                        }).catch(err => {
                            console.log(err)
                        })
                    }
                    else if (result) {
                        console.log('Ya existe')
                    }
                })
            }
            i++
        }
        res.send('guardado en la BD');
        console.log('Datos guardados')
    })
}

//Actualiza los repositorios en la BD
// * localhost:3001/repositorios/actualizar
const updateRepositorios = async (req, res) => {
    // a funtion to update the data from the api
    let username = req.params.username;
    obtener(username).then(data => {
        let i = 0
        while (data[i] != null) {
            if (data[i].name === username) {
                console.log('Omitiendo repositorio')
            }
            else {
                Repositories.findOneAndUpdate({ name: data[i].name },
                    {
                        name: data[i].name,
                        url: data[i].html_url,
                        description: data[i].description,
                        lenguages: data[i].lenguages,
                        topics: data[i].topics
                    },
                    { new: true }, function (err, result) {
                        if (result === null) {
                            console.log('No existe')
                        }
                        else if (result) {
                            console.log('Actualizado')
                        }
                    })
            }
            i++
        }
        res.send('actualizado en la BD');
        console.log('Datos actualizados')
    })
}

//elimina repositorios de la BD
// * localhost:3001/repositorios/eliminar
const deleteRepositorios = (req, res) => {
    //a funtion to delete the data from the bd
    Repositories.deleteMany({}, (err, result) => {
        if (err) {
            console.log('ocurrio un error al eliminar datos de la BD')
        }
        else {
            console.log('Datos eliminados de la BD')
            res.send(result)
        }
    })
}


module.exports = { getRepositorios, createRepositorios, updateRepositorios, deleteRepositorios, getRepositoriosBD }