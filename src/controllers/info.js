const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const Info = require('../models/info.js')
const User = require('../models/user.js')
let signin = false


const signInfo = (req, res) => {
    res.send({
        token: 'test123'
    });
    console.log(req.body)
}
// * localhost:3001/info/auth
const authInfo = (req, res) => {
    // a function to login the user
    console.log(req.body)
    User.find({ username: req.body.username }, (err, result) => {
        if (err) {
            console.log('Ha ocurrido un error al obtener datos de InfoDB')
        }
        else {
            if (result.length > 0) {
                if (result[0].password === req.body.password) {
                    console.log('usuario autenticado')
                    //res.json({ message: 'usuario autenticado' })
                    req.session.loggedin = true;
                    req.session.username = req.body.username;
                    signin = true
                    res.send({
                        token: 'test123'
                    });
                }
                else {
                    console.log('contraseña incorrecta')
                    res.json({ message: 'contraseña incorrecta' })
                }
            }
            else {
                console.log('el usuario no existe')
                res.json({ message: 'el usuario no existe' })
            }
        }
    })
}

const getInfo = (req, res) => {
    Info.find({}, (err, result) => {
        if (err) {
            console.log('Ha ocurrido un error al obtener datos de InfoDB')
        }
        else {
            console.log('enviando info')
            res.json(result)
        }
    })

}



const postInfo = (req, res) => {
    // a funtion to post no repeat data on the database
    console.log(req.body)
    Info.find({ username: req.body.username }, (err, result) => {
        if (err) {
            console.log('Ha ocurrido un error al obtener datos de InfoDB')
        }
        else {
            if (result.length > 0) {
                console.log('el usuario ya existe')
                res.json({ message: 'el usuario ya existe' })
            }
            else {
                console.log('creando usuario')
                var info = new Info(
                    {
                        username: req.body.username,
                        lastname: req.body.lastname,
                        job: req.body.job,
                        aboutme: req.body.aboutme,
                        knowledges: req.body.knowledges,
                        email: req.body.email,
                        phone: req.body.phone,
                        usergithub: req.body.usergithub,
                    }
                )
                info.save()
                console.log(info)
                res.json({ message: 'usuario creado' })
            }
        }
    })
}


const updateInfo = (req, res) => {
    // a function to update the data on the database
    console.log(req.body)

    Info.find({ nuser: 1 }, (err, result) => {
        if (err) {
            console.log('Ha ocurrido un error al obtener datos de InfoDB')
        }
        else {
            if (result.length > 0) {
                console.log('el usuario existe')
                Info.updateOne({ nuser: 1 }, {
                    username: req.body.username,
                    lastname: req.body.lastname,
                    job: req.body.job,
                    aboutme: req.body.aboutme,
                    knowledges: req.body.knowledges,
                    email: req.body.email,
                    phone: req.body.phone,
                    usergithub: req.body.usergithub,
                }, (err, result) => {
                    if (err) {
                        console.log('Ha ocurrido un error al actualizar datos de InfoDB')
                    }
                    else {
                        console.log('usuario actualizado')
                        res.json({ message: 'usuario actualizado' })
                    }
                })
            }
            else {
                console.log('el usuario no existe')
                res.json({ message: 'el usuario no existe' })
            }
        }
    })


}


const deleteInfo = (req, res) => {
    // a function to delete the data on the database
    console.log(req.body)
    Info.find({ nuser: 1 }, (err, result) => {
        if (err) {
            console.log('Ha ocurrido un error al obtener datos de InfoDB')
        }
        else {
            if (result.length > 0) {
                console.log('el usuario existe')
                Info.deleteOne({ username: req.body.username }, (err, result) => {
                    if (err) {
                        console.log('Ha ocurrido un error al eliminar datos de InfoDB')
                    }
                    else {
                        console.log('usuario eliminado')
                        res.json({ message: 'usuario eliminado' })
                    }
                })
            }
            else {
                console.log('el usuario no existe')
                res.json({ message: 'el usuario no existe' })
            }
        }
    })
}


module.exports = { getInfo, postInfo, updateInfo, deleteInfo, authInfo, signInfo }