
//importando fetch
const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));

module.exports = function obtener(username) {
    let headersList = {
        method: 'GET',
        headers: {
            "Accept": "*/*",
            "User-Agent": "*/*"
        }
    }
    return fetch(`https://api.github.com/users/${username}/repos`, headersList).then(response => response.json());
}