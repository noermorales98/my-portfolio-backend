/*
 * Routes
 */

const express = require('express')
const router = express.Router()
//importando controladores
const repositoriosController = require('../controllers/repositories')

//Obtiene repositorios desde Github
router.get('/', repositoriosController.getRepositorios)

//Obtiene repositorios desde la BD
router.get('/obtener', repositoriosController.getRepositoriosBD)

//Crea y almacena los repositorios en la BD
router.post('/guardar/:username', repositoriosController.createRepositorios)

//Actualiza los repositorios en la BD
router.put('/actualizar/:username', repositoriosController.updateRepositorios)

//elimina repositorios de la BD
router.delete('/eliminar', repositoriosController.deleteRepositorios)

module.exports = router


