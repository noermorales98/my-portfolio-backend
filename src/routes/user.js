const express = require('express')
const router = express.Router()

const userController = require('../controllers/user.js')

router.get('/', userController.getUser)
router.post('/update', userController.postUser)


module.exports = router