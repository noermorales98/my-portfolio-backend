const express = require('express')
const router = express.Router()

const infoController = require('../controllers/info.js')

router.get('/', infoController.getInfo)
router.post('/auth', infoController.authInfo)
router.post('/post', infoController.postInfo)
router.put('/update', infoController.updateInfo)
router.get('/delete', infoController.deleteInfo)
router.post('/sign', infoController.signInfo)


module.exports = router