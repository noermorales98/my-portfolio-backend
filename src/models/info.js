/*
 * Models
    Información del usuario
 */

const { default: mongoose } = require('mongoose')
const moongose = require('mongoose')
const Schema = moongose.Schema

const infoSchema = new Schema({
    username: {
        type: String,
        require: true,
        unique: true
    },
    lastname: {
        type: String,
        require: true,
        unique: true
    },
    job: {
        type: String,
        require: true,
        unique: true
    },
    aboutme: {
        type: String,
        require: true
    },
    knowledges: {
        type: Array,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    phone: {
        type: Number,
        require: true,
        unique: true
    },
    usergithub:{
        type: String,
        require: true,
        unique: true
    },
    nuser:{
        type: Number,
        default:1
    }
})

const Info = mongoose.model('Info', infoSchema)
module.exports = Info