/*
 * Models
    Información del usuario
 */

    const { default: mongoose } = require('mongoose')
    const moongose = require('mongoose')
    const Schema = moongose.Schema
    
    const userSchema = new Schema({
        username: {
            type: String,
            require: true,
            unique: true
        },
        password: {
            type: String,
            require: true,
            unique: true
        }
    })
    
    const User = mongoose.model('User', userSchema)
    module.exports = User