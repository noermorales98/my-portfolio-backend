/*
    * Models
    Creacion del modelo de la BD
*/

//importando moongose
const { default: mongoose } = require('mongoose')
const moongose = require('mongoose')
//especificando la estructura del modelo
const Schema = moongose.Schema

const repositoriesSchema = new Schema({
    name: {
        type:String,
        required: true,
        unique: true
    },
    url: String,
    description: String,
    lenguages: String,
    topics: Array
})


const Repositories = mongoose.model('Repositories', repositoriesSchema)

module.exports = Repositories