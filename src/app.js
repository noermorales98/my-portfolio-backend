const conexion = require('./conexion')
const cors = require('cors')
const bodyParser = require('body-parser')
const { use } = require('./routes/repositories.js')
const session = require('express-session');
const express = require('express')
const app = express()
class App {
    constructor(port) {
        this.port = port
    }
    init(port) {
        app.get('/', (req, res) => {
            res.json({ message: "Repositorios" });
        });
        app.listen(port, () => {
            console.log('corriendo en http://localhost:' + port)
        });
        //para autenticacion
        app.use(session({
            secret: 'secret',
            resave: true,
            saveUninitialized: true
        }));
        //usar json
        app.use(bodyParser.urlencoded({ extended: true }))
        app.use(bodyParser.json());
        app.use(cors({origin:/http:\/\/localhost/}))

    }
    usar(name, ruta) {
        app.use(`/${name}`, ruta)
    }
}

module.exports = App