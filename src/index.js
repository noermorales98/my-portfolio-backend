const App = require('./app')
const app = new App()

app.init(3001)
app.usar('info',require('./routes/info.js'))
app.usar('repositorios',require('./routes/repositories.js'))
app.usar('user', require('./routes/user.js'))