# Rutas básicas
Express nos deja definir las rutas con nuestro objeto app. También podemos manejar métodos HTTP como GET, POST, PUT/PATCH, y DELETE.

Está es la forma más simple de definir rutas, pero a medida que nuestra aplicación crezca, necesitaremos más organización para nuestras rutas.

## EXPRESS.ROUTER()
express.Router() actúa como una mini aplicación. Puedes crear una instancia (como hicimos con Express) y luego definir rutas con ella. Vamos a ver un ejemplo.

Debajo nuestro app.get() route dentro del server.js, añade lo siguiente. Vamos a 1. llamar una instancia del router 2. aplicarle rutas a esa instancia 3. y luego añadir estas rutas a nuestra app principal.

```javascript
//creamos las rutas para la parte de admin
 
//instanciamos router
var adminRouter = express.Router();
 
//página principal del admin, panel de administración/dashboard (http://localhost:1337/admin)
adminRouter.get('/', function(req, res) {
 res.send('¡Soy el panel de administración!');
});
 
//users page (http://localhost:1337/admin/users)
adminRouter.get('/users', function (req, res) {
 res.send('¡Muestro todos los usuarios!');
});
 
//posts page (http://localhost:1337/admin/users)
adminRouter.get('/posts', function(req, res) { 
 res.send('¡Muestro todos los posts!');
});
 
//aplicamos las rutas a nuestra aplicación, app
app.use('/admin', adminRouter);
```

Ahora podemos acceder al panel de administración en http://localhost:1337/admin y a las sub-páginas en http://localhost:1337/admin/users y http://localhost:1337/admin/posts.