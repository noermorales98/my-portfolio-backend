# middleware

El middleware es el software que brinda servicios y funciones comunes a las aplicaciones, además de lo que ofrece el sistema operativo. Generalmente, se encarga de la gestión de los datos, los servicios de aplicaciones, la mensajería, la autenticación y la gestión de las API.

Básicamente un middleware es una función que se ejecuta entre la petición(request) y la respuesta (response) de tu servidor, por lo tanto, puede ejecutarse antes o después de llegar al controlador, el concepto siempre es el mismo pero asegúrate de estar dentro del ciclo de vida de la petición.

```javascript
const my_middleware = (req, res, next)=>{
    console.log('ejecutando el middleware')
    next()
}

app.use(my_middleware)
```

La razón de utilizar middlewares es por una lógica muy simple, los controladores no pueden hacer ni tener todo, con esto me refiero a que existe lógica que debe realizarse mucho antes o después para en multiples lugares, imagina que vas en contra de todo y pones un pedazo de código en cada una de las rutas de tus controladores, que pasaría si necesitas realizar un pequeño cambio, esto fácil es un cambio en todos esos lugares, por consecuencia la legibilidad de tu proyecto será severamente afectada.