# Run project

```cmd
npm run server
```

# ¿Que es express.js?
Express.js es el framework backend más popular para Node.js, y es una parte extensa del ecosistema JavaScript.

Está diseñado para construir aplicaciones web de una sola página, multipágina e híbridas, también se ha convertido en el estándar para desarrollar aplicaciones backend con Node.js

## routes
Puedes usar rutas en otros directorios para no tenerlos todos en un mismo archivo.

express.Router() actúa como una mini aplicación. Puedes crear una instancia y luego definir rutas con ella.

```javascript
const router = express.Router()

router.get('/repositorios', (req, res) => {
    res.send('repositorios');
})
```

## controllers
los controladores manejan toda la lógica detrás de los parámetros de solicitud de validación, consulta, envío de respuestas con los códigos correctos. Es bueno para no tener toda la lógica de programación en un solo archivo, se puede almacenar en otra ubicación y ser llamado.

## middleware
Es una funcion que es ejecutada luego de que el servidor resibe una petición y antes de que este emita una respuesta.

Básicamente un middleware es una función que se ejecuta entre la petición(request) y la respuesta (response) de tu servidor, por lo tanto, puede ejecutarse antes o después de llegar al controlador, el concepto siempre es el mismo pero asegúrate de estar dentro del ciclo de vida de la petición.

```javascript
const my_middleware = (req, res, next)=>{
    console.log('ejecutando el middleware')
    next()
}

app.use(my_middleware)
```
